<?php

namespace Drupal\viewerjs_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'viewerjs_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "viewerjs_formatter",
 *   module = "viewerjs_formatter",
 *   label = @Translation("List of files - ViewerJS"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class ViewerJsFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'viewerjs_formatter_type' => 'ul',
      'viewerjs_formatter_class' => 'viewerjs-file-list',
      'viewerjs_formatter_width' => '100%',
      'viewerjs_formatter_height' => '600px',
      'viewerjs_formatter_download_link' => TRUE,
      'viewerjs_formatter_download_link_after_iframe' => FALSE,
      'viewerjs_formatter_list_style_none' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $settings = $this->getSettings();

    $form['viewerjs_formatter_type'] = [
      '#title' => $this->t('List type'),
      '#type' => 'select',
      '#options' => [
        'ul' => $this->t('Unordered HTML list (ul)'),
        'ol' => $this->t('Ordered HTML list (ol)'),
      ],
      '#default_value' => $settings['viewerjs_formatter_type'],
      '#required' => TRUE,
    ];

    $form['viewerjs_formatter_class'] = [
      '#title' => $this->t('List classes'),
      '#type' => 'textfield',
      '#size' => 40,
      '#description' => $this->t('A CSS class to use in the markup for the field list.'),
      '#default_value' => $settings['viewerjs_formatter_class'],
      '#required' => FALSE,
    ];

    $form['viewerjs_formatter_width'] = [
      '#title' => $this->t('ViewerJS iframe width'),
      '#type' => 'textfield',
      '#size' => 40,
      '#description' => $this->t('Width of the ViewerJS iframe.'),
      '#default_value' => $settings['viewerjs_formatter_width'],
      '#required' => FALSE,
    ];

    $form['viewerjs_formatter_height'] = [
      '#title' => $this->t('ViewerJS iframe height'),
      '#type' => 'textfield',
      '#size' => 40,
      '#description' => $this->t('Height of the ViewerJS iframe.'),
      '#default_value' => $settings['viewerjs_formatter_height'],
      '#required' => FALSE,
    ];

    $form['viewerjs_formatter_download_link'] = [
      '#title' => $this->t('Display document file download link.'),
      '#type' => 'checkbox',
      '#description' => $this->t('Enable to display a download link for the document file.'),
      '#default_value' => $settings['viewerjs_formatter_download_link'],
      '#required' => FALSE,
    ];

    $form['viewerjs_formatter_download_link_after_iframe'] = [
      '#title' => $this->t('Show document file download link after the iframe.'),
      '#type' => 'checkbox',
      '#description' => $this->t('Enable to place the document download link after the iframe.'),
      '#default_value' => $settings['viewerjs_formatter_download_link_after_iframe'],
      '#required' => FALSE,
    ];

    $form['viewerjs_formatter_list_style_none'] = [
      '#title' => $this->t('Clean file list style.'),
      '#type' => 'checkbox',
      '#description' => $this->t('Apply no bullets on unordered list style.'),
      '#default_value' => $settings['viewerjs_formatter_list_style_none'],
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    switch ($settings['viewerjs_formatter_type']) {
      case 'ul':
        $summary[] = $this->t('Unordered HTML list');
        break;

      case 'ol':
        $summary[] = $this->t('Ordered HTML list');
        break;
    }

    if ($settings['viewerjs_formatter_class']) {
      $summary[] = $this->t('CSS Class: @class', ['@class' => $settings['viewerjs_formatter_class']]);
    }

    if ($settings['viewerjs_formatter_width']) {
      $summary[] = $this->t('ViewerJS iframe width: @width', ['@width' => $settings['viewerjs_formatter_width']]);
    }

    if ($settings['viewerjs_formatter_height']) {
      $summary[] = $this->t('ViewerJS iframe height: @height', ['@height' => $settings['viewerjs_formatter_height']]);
    }

    if ($settings['viewerjs_formatter_download_link']) {
      $summary[] = $this->t('Display document file download link.');
    }
    else {
      $summary[] = $this->t('Do not display document file download link.');
    }

    if ($settings['viewerjs_formatter_download_link_after_iframe']) {
      $summary[] = $this->t('Show document file download link after the iframe.');
    }
    else {
      $summary[] = $this->t('Show document file download link before the iframe.');
    }

    if ($settings['viewerjs_formatter_list_style_none']) {
      $summary[] = $this->t('No bullets for unordered file list.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();

    $list_elements = [];
    if (!$items->isEmpty()) {
      $elements = [];
      global $base_path;
      $viewerjs_path = 'libraries/viewerjs';
      $iframe_width = $settings['viewerjs_formatter_width'];
      $iframe_height = $settings['viewerjs_formatter_height'];
      $field_name = $items->getName();
      $iframe_classes = 'viewerjs_iframe viewerjs_iframe-' . $field_name;
      $itemcount = 0;

      foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
        $itemcount++;
        /**
         * @var \Drupal\file\FileInterface $file
         * @var \Drupal\file\Plugin\Field\FieldType\FileItem $item
         */
        $item = $file->_referringItem;
        if ($item->isDisplayed() && $item->entity) {
          $elemcount = 0;

          $description = '';
          if ($item->getFieldDefinition()->getSetting('description_field')) {
            $description = $item->description;
          }
          if (empty($description)) {
            $description = $file->get('filename')->value;
          }
          $file_link_element = [
            '#theme' => 'file_link',
            '#file' => $file,
            '#description' => $description,
            '#cache' => [
              'tags' => $file->getCacheTags(),
            ],
          ];

          if ($settings['viewerjs_formatter_download_link'] &&
              !($settings['viewerjs_formatter_download_link_after_iframe'])) {
            // Display document file download link.
            $elements[$delta][$elemcount] = $file_link_element;
            $elemcount++;
          }

          // Set up ViewerJS iframes for each file.
          $the_file_path = $file->createFileUrl();
          $iframe_id = 'viewerjs_iframe-' . $field_name . '-' . $itemcount;
          $elements[$delta][$elemcount] = [
            '#type' => 'html_tag',
            '#tag' => 'iframe',
            '#attributes' => [
              'src' => $base_path . $viewerjs_path . '/#' . $the_file_path,
              'class' => $iframe_classes,
              'id' => $iframe_id,
              'name' => $iframe_id,
              'height' => $iframe_height,
              'width' => $iframe_width,
              'allow' => 'fullscreen',
              'allowfullscreen' => 'true',
              'mozallowfullscreen' => 'true',
              'webkitallowfullscreen' => 'true',
              'title' => $description,
            ],
            '#cache' => [
              'tags' => $file->getCacheTags(),
            ],
          ];

          if ($settings['viewerjs_formatter_download_link'] &&
              ($settings['viewerjs_formatter_download_link_after_iframe'])) {
            $elemcount++;
            // Display document file download link.
            $elements[$delta][$elemcount] = $file_link_element;
          }

          // Pass field item attributes to the theme function.
          if (isset($item->_attributes)) {
            $elements[$delta] += ['#attributes' => []];
            $elements[$delta]['#attributes'] += $item->_attributes;
            // Unset field item attributes since they have been included in the
            // formatter output and shouldn't be rendered in the field template.
            unset($item->_attributes);
          }
        }
      }

      $list_classes = explode(' ', $settings['viewerjs_formatter_class']);
      $list_elements = [
        '#theme' => 'item_list',
        '#items' => $elements,
        '#list_type' => $settings['viewerjs_formatter_type'],
        '#attributes' => [
          'class' => $list_classes,
          'role' => 'list',
          'style' => 'list-style: ' . ($settings['viewerjs_formatter_list_style_none'] ? 'none;' : 'inherit;'),
        ],
      ];
      if (($settings['viewerjs_formatter_type'] == 'ol') ||
          ($settings['viewerjs_formatter_list_style_none'] == FALSE)) {
        unset($list_elements['#attributes']['style']);
      }
    }

    return $list_elements;
  }

}
