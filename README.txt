Introduction
------------
A field formatter rendering a list of files with the ViewerJS library.

Requirements
------------
* Drupal core's File module
* ViewerJS (https://viewerjs.org/getit) installed in libraries/viewerjs

Extract the contents of the ViewerJS distribution archive and
copy the contents of the ViewerJS folder to libraries/viewerjs .

Installation
------------
* Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8

Configuration
-------------
There is no special configuration for this module. You may configure
it as you would any other field formatter on the Manage display tab.
